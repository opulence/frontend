FROM python:3.6
ADD . /app
WORKDIR /app/server
RUN pip install -r requirements.txt
RUN pip install gunicorn
EXPOSE 8000
CMD ["gunicorn", "-b", "0.0.0.0:8000", "run"]
