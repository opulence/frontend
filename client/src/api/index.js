import axios from 'axios'

let $axios = axios.create({
  baseURL: process.env.VUE_APP_API_URL + '/api/',
  timeout: 40000
})

$axios.interceptors.request.use(function (config) {
  config.headers['Authorization'] = 'Fake Token'
  return config
})

$axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  console.log(error)
  return Promise.reject(error)
})


export default {
    getCollectors() {
        return $axios.get('collectors')
            .then(response => response.data)
    },
    getFacts() {
        return $axios.get('facts')
            .then(response => response.data)
    },
    executeCollector(name, facts) {
        let data = {
            collector_name: name,
            facts: facts
        };
        return $axios.post('scans/quick_scan', data)
            .then(response => response.data)          
    },
    getResults() {
        return $axios.get('scans')
            .then(response => response.data)
    },
    removeResults(id) {
        return $axios.post('scans/remove', {id: id})
    },
    treeResults(scan_id) {
        return $axios.get('scans/' + scan_id + '/tree')
    },
    flatResults(scan_id) {
        return $axios.get('scans/' + scan_id + '/flat')
    },
    sendScan(data) {
        return $axios.post('scans/launch', data)
            .then(response => response.data)          
    },
}
