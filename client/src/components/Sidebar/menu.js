export default {
	"routes": [
        {
            "menu_title": "message.collectors",
            "menu_icon": "ti-magnet",
            "active": false,
            "child_routes": [
                {
                    "route_name": "collectors",
                    "path": "/collectors/list",
                    "menu_title": "message.collectors_list"
                },
                {
                    "route_name": "CollectorsResults",
                    "path": "/collectors/results",
                    "menu_title": "message.results"
                },                
            ]       
        },
        {
            "menu_title": "message.facts",
            "menu_icon": "ti-files",
            "active": false,
            "child_routes": [
                {
                    "route_name": "list",
                    "path": "/facts/list",
                    "menu_title": "message.fact_list"
                },
            ]       
        },
        {
            "menu_title": "message.scan",
            "menu_icon": "ti-target",
            "active": false,
            "child_routes": [
                {
                    "route_name": "scan_launch",
                    "path": "/scan/launch",
                    "menu_title": "message.launch_scan"
                },
            ]       
        }

	]
}