import Spinner from 'vue-spinner-component/src/Spinner.vue';
import VuePerfectScrollbar from "vue-perfect-scrollbar";
import CountUp from 'Components/CountUp/CountUp';
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";
import AppCard from 'Components/AppCard/AppCard';
import AppSectionLoader from 'Components/AppSectionLoader/AppSectionLoader';

const GlobalComponents = {
	install(Vue) {
		Vue.component('countUp', CountUp);
		Vue.component('appCard', AppCard);
		Vue.component('vuePerfectScrollbar', VuePerfectScrollbar);
		Vue.component('pageTitleBar', PageTitleBar);
		Vue.component('appSectionLoader', AppSectionLoader);
		Vue.component('Spinner', Spinner);
	}
}

export default GlobalComponents