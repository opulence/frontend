export function statusCodeToLabel(code) {
    var codes = {
        0: ["Undefined", "warning"],
        10: ["Empty", "danger"],
        20: ["Ready", "primary"],
        30: ["Started", "primary"],
        40: ["Finished", "success"],
        100: ["Error", "danger"],
        101: ["Invalid input", "danger"],
        200: ["Cancelled", "warning"],
        300: ["Rate limited","warning"]
    };
    if (code in codes) { return codes[code] }
    else { return ["???","warning"] }
}

export function statusIsErrored(code) {
    return code >= 100
}

export function hexToRgbA(hex, alpha) {
    var c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length === 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x' + c.join('');
        return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
    }
    throw new Error('Bad Hex');
}

export function textTruncate(str, length, ending) {
    if (length == null) {
        length = 100;
    }
    if (ending == null) {
        ending = '...';
    }
    if (str.length > length) {
        return str.substring(0, length - ending.length) + ending;
    } else {
        return str;
    }
}

export function factCategoryIcon(category) {
    var categories = {
        "fact.infrastructure": "bars"
    }

    if (category in categories) { return categories[category] }
    else { return "bomb" }
}