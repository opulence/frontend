//messages.js
export default {
  fact_list_breadcrumb: "facts / list",
  collectors_list_breadcrumb: "collectors / list",
  collector_results_breadcrumb: "collectors / results",
  scan_breadcrumb: "scan",
  graph_breadcrumb: "graphs",


  facts: "Facts",
  collectors: "Collectors",

  fact_details: "Fact details",
  fact_list: "Facts list",
  collectors_list: "Collectors list",

  default_value: "Default value",
  mandatory: "Mandatory",

  collectors_up: "Collector(s) UP",
  collectors_down: "Collector(s) DOWN",
  execute_collector: "Launch collector",

  name: "Name",
  details: "Details",

  status: "State",
  description: "Description",
  launch: "Launch",
  category: "Category",
  author:  "Author",
  version: "Version",
  dark_theme: "Dark theme",
  results: "Results",
  search: "Search",
  delete: "Remove",
  cancel: "Cancel",
  rows_selected: "rows selected",
  error: "Error",
  success: "Success",

  collectors_name: "Collector's name",
  identifiant: "Identifier",
  number_of_results: "Number of results",
  duration: "Duration",
  launched_since: "Launched",
  reload_results: "Reload results",
  results_reloaded: "Results updated !",
  validate: "Validate",

  see_results: "See results",

  scan: "Scan",
  launch_scan: "Launch scan",

  choose_scan_type: "Choose scan type",
  add_inputs: "Add input(s)",
  please_select_option: "Please select an option",
  next: "Next",
  previous: "Back",
  send: "Send",
  edit_input: "Edit input",

  dataviz: "Data visualisation",

  graph: "Graph",
  tree: "Tree",
  cluster: "Cluster",
  table: "Table",

  summary: "Summary",

  show: "Show",
  hide: "Hide",
  the_details: "details",
  click_to_select_fact_input: "Select facts:",
  no_data: "No data found",
    active_scanning: "Active scanning",
  yes: "yes",
  no: "no",
  scan_type: "Scan type",
  scan_launched: "Scan successfully launched!",



  full_scan: "Full Scan",
  step_1: "First step",
  step_2: "Second step",
  step_3: "Third step",
  step_final: "Last step",
}