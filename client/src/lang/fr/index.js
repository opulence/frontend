//messages.js

export default {
  fact_list_breadcrumb: "facts / liste",
  collectors_list_breadcrumb: "collecteurs / liste",
  collector_results_breadcrumb: "collecteurs / resultats",
  scan_breadcrumb: "scan",
  graph_breadcrumb: "graphiques",

  facts: "Facts",
  collectors: "Collecteurs",

  fact_details: "Detail du fact",
  fact_list: "Liste des facts",
  collectors_list: "Liste des collecteurs",

  default_value: "Valeur par Defaut",
  mandatory: "Obligatoire",

  collectors_up: "Collecteur(s) en ligne",
  collectors_down: "Collecteur(s) hors-ligne",
  execute_collector: "Lancer le collecteur",

  name: "Nom",
  details: "Détails",
  status: "Status",
  description: "Description",
  launch: "Lancer",
  category: "Catégorie",
  author:  "Auteur",
  version: "Version",
  dark_theme: "Thème sombre",
  results: "Resultats",
  search: "Rechercher",
  delete: "Supprimer",
  cancel: "Annuler",
  rows_selected: "lignes séléctionnées",
  error: "Erreur",
  success: "Succés",
  validate: "Valider",

  collectors_name: "Nom du collecteur",
  identifiant: "Identifiant",
  number_of_results: "Nombre de résultats",
  duration: "Durée",
  launched_since: "Lancé le",
  reload_results: "Recharger les résultats",
  results_reloaded: "Resultats mis à jour !",
  see_results: "Voir les résultats",

  scan: "Scan",
  launch_scan: "Lancer un scan",

  please_select_option: "Veuillez sélectionner une option",
  please_select_a_scan_type: "Veuillez sélectionner un type de scan",

  choose_scan_type: "Selectionnez un scan",
  add_inputs: "Ajouter des entrées",
  next: "Suivant",
  previous: "Précédent",
  send: "Envoyer",
  edit_input: "Modifier l'entrée",

  dataviz: "Visualisation des données",



  graph: "Graphique",
  tree: "Arbre",
  cluster: "Grappes",
  table: "Table",

  filter: "Filtrer:",
  filter_by: "Filter par: ",
  count: "Nombre",

  summary: "Résumé",

  show: "Montrer",
  hide: "Cacher",
  the_details: "les détails",
  click_to_select_fact_input: "Selectionnez les facts:",
  no_data: "Pas de résultats",
  active_scanning: "Scan actif",
  yes: "oui",
  no: "non",
  scan_type: "Type de scan",
  scan_launched: "Scan lancé avec succés!",



  full_scan: "Full Scan",
  step_1: "Première étape",
  step_2: "Deuxieme étape",
  step_3: "Troisième étape",
  step_final: "Dernière étape",
}