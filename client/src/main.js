import "babel-polyfill";
import Vue from "vue";
import VueSweetAlert from "vue-sweetalert";
import BootstrapVue from "bootstrap-vue";
import App from "./App";
import router from "./router";
import Notifications from "vue-notification";
import velocity from "velocity-animate";
import VueBreadcrumbs from "vue2-breadcrumbs";
import VueI18n from "vue-i18n";
import fullscreen from "vue-fullscreen";

import "./lib/adminifyScripts";
import GlobalComponents from "./globalComponents";
import { store } from "./store/store";
import "./lib/adminifycss";
import messages from "./lang";

import VueGoodTablePlugin from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'

import Nprogress from "nprogress";





router.beforeEach((to, from, next) => {
    Nprogress.start();
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // this route requires auth, check if logged in
      // if not, redirect to login page.
      if (localStorage.getItem('user') === null) {
        next({
          path: "/session/login",
          query: { redirect: to.fullPath }
        })
      } else {
        next()
      }
    } else {
      next() // make sure to always call next()!
   }
});

// navigation guard after each
router.afterEach((to, from) => {
  Nprogress.done();
  setTimeout(() => {
    const contentWrapper = document.querySelector(".base-container");
    if (contentWrapper !== null) {
      contentWrapper.scrollTop = 0;
    }
  }, 200);
});



Vue.use(fullscreen)
Vue.use(VueSweetAlert);
Vue.use(BootstrapVue);
Vue.use(Notifications, { velocity });
Vue.use(VueBreadcrumbs);
Vue.use(VueI18n);
Vue.use(VueGoodTablePlugin);
Vue.use(require('vue-moment'));

Vue.use(GlobalComponents);


Vue.config.productionTip = true;

const i18n = new VueI18n({
	locale: store.getters.selectedLocale.locale,
	messages
})

new Vue({
	store,
  	i18n,
	router,
	render: h => h(App),
	components: { App }
}).$mount('#app')
