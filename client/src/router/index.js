import Vue from 'vue'
import Router from 'vue-router'

import Full from '@/container/Full'


const FactList = () => import('Views/facts/FactList')
const CollectorsList = () => import('Views/collectors/CollectorsList')

const CollectorsResults = () => import('Views/results/CollectorResults')

const LaunchScan = () => import('Views/scan/LaunchScan')

const DataViz = () => import('Views/dataViz/index')

const Test = () => import('Views/test')
Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			component: Full,
			redirect: '/collectors/list',
			children: [
                {
                    name: "Collectors",
                    path: '/collectors/list',
                    component: CollectorsList,
                    meta: {
                        requiresAuth: false,
                        title: 'message.collectors.list',
                        breadcrumb: 'message.collectors_list_breadcrumb'
                    }
                },
                {
                    name: "test",
                    path: '/test',
                    component: Test,
                    meta: {
                        requiresAuth: false,
                        title: 'test',
                        breadcrumb: 'test'
                    }
                },
                {
                    name: "List",
                    path: '/facts/list',
                    component: FactList,
                    meta: {
                        requiresAuth: false,
                        title: 'message.fact.list',
                        breadcrumb: 'message.fact_list_breadcrumb'
                    }
                },
                {
                    name: "CollectorsResults",
                    path: '/collectors/results',
                    component: CollectorsResults,
                    meta: {
                        requiresAuth: false,
                        title: 'message.collectors.result',
                        breadcrumb: 'message.collector_results_breadcrumb'
                    },
                    props: true              
                },
                {
                    name: "LaunchScan",
                    path: '/scan/launch',
                    component: LaunchScan,
                    meta: {
                        requiresAuth: false,
                        title: 'message.scan',
                        breadcrumb: 'message.scan_breadcrumb'
                    },
                    props: true              
                },
                {
                    name: "DataViz",
                    path: '/graphs',
                    component: DataViz,
                    meta: {
                        requiresAuth: false,
                        title: 'message.dataviz',
                        breadcrumb: 'message.graph_breadcrumb'
                    },
                    props: true              
                }

			]
        }]
})
