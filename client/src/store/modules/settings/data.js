export const languages = [
	{
		name: "English",
		icon: "en",
		locale: "en"
	},
	{
		name: "French",
		icon: "fr",
		locale: "fr"
	}
]

export const sidebarBackgroundImages = [
	{
		id: 1,
		url: '/static/img/logo.jpg'
	},
	{
		id: 2,
		url: '/static/img/logo.jpg'
	},
	{
		id: 3,
		url: '/static/img/logo.jpg'
	},
]

export const themeColor = [
	{
		extraClass: "default",
		iconColor: "text-primary",
		text: "Default"
	},
	{
		extraClass: "orange",
		iconColor: "text-warning",
		text: "Orange"
	},
	{
		extraClass: "green",
		iconColor: "text-success",
		text: "Green"
	},
	{
		extraClass: "red",
		iconColor: "text-danger",
		text: "Red"
	}
]