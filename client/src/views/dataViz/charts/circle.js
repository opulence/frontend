export function renderCircleCollapsible(params) {
  var attrs = {
    id: 'id' + Math.floor(Math.random() * 1000000),
    svgWidth: 960,
    svgHeight: 600,
    marginTop: 0,
    marginBottom: 5,
    marginRight: 0,
    marginLeft: 30,
    nodeRadius: 18,
    container: 'body',
    distance: 50,
    hiddenChildLevel: 1,
    linkColor: '#0F4C81',
    hoverOpacity: 0.2,
    maxTextDisplayZoomLevel: 1,
    lineStrokeWidth: 1.5,
    data: null,
    hoverCallback: null,
  };


  var updateData;
  var filter;

  var main = function (selection) {
    selection.each(function scope() {
      var calc = {}
      calc.chartLeftMargin = attrs.marginLeft;
      calc.chartTopMargin = attrs.marginTop;
      calc.chartWidth = attrs.svgWidth - attrs.marginRight - calc.chartLeftMargin;
      calc.chartHeight = attrs.svgHeight - attrs.marginBottom - calc.chartTopMargin;
    var viewerWidth = $(document).width()
    var viewerHeight = $(document).height() / 1.2;
  
    var diameter = 500
    var margin = 10
    var svg = d3.select(attrs.container).append("svg").attr("width", viewerWidth)
                                                .attr("height", viewerHeight)
                                                .attr("class", "overlay")

    var g = svg.append("g")
    .attr("transform", "translate(" + diameter * 1.4 + "," + diameter / 2 + ")");

var color = d3.scaleLinear()
    .domain([-1, 5])
    .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
    .interpolate(d3.interpolateHcl);

var pack = d3.pack()
    .size([diameter - margin, diameter - margin])
    .padding(2);

    var root = {"name":"aze","children":[{"name":"2","children":[{"name":"p23.3","children":[{"name":"IFT172","children":[{"name":"undefined"},{"name":"aaaacxi7gjs3gascvqjaabaaaq"},{"name":"aaaacxi7gjs3eascvqjaabaaci","size":6},{"name":"aaaacxi7gjs3gascvqjaabaabe","size":6},{"name":"aaaacxi7gjs3gascvqjaabaacm",},{"name":"aaaacxi7gjs3eascvqjaabaaca",},{"name":"aaaacxi7gjs3gascvqjaabaace","size":7},{"name":"aaaacxi7gjs3eascvqjaabaab4","size":5},{"name":"aaaacxi7gjs3gascvqjaabaaae","size":3},{"name":"aaaacxi7gjs3eascvqjaabaace","size":9},{"name":"aaaacxi7gjs3eascvqjaabaacq","size":9},{"name":"aaaacxi7gjs3gascvqjaabaaa4","size":4},{"name":"aaaacxi7gjs3eascvqjaabaaau","size":13},{"name":"aaaacxi7gjs3gascvqjaabaaay","size":5},{"name":"aaaacxi7gjs3eascvqjaabaaby","size":6},{"name":"aaaacxi7gjs3eascvqjaabaabq","size":5},{"name":"aaaacxi7gjs3gascvqjaabaaam","size":6},{"name":"aaaacxi7gjs3iascvqjaabaaaq","size":6},{"name":"aaaacxi7gjs3eascvqjaabaabi","size":5},{"name":"aaaacxi7gjs3iascvqjaabaaae","size":3},{"name":"aaaacxi7gjs3gascvqjaabaacu","size":2},{"name":"aaaacxi7gjs3eascvqjaabaabm","size":3},{"name":"aaaacxi7gjs3gascvqjaabaabu","size":3},{"name":"aaaacxi7gjs3iascvqjaabaaai","size":7},{"name":"aaaacxi7gjs3gascvqjaabaac4","size":3},{"name":"aaaacxi7gjs3gascvqjaabaabq","size":4},{"name":"aaaacxi7gjs3eascvqjaabaaay","size":6},{"name":"aaaacxi7gjs3gascvqjaabaabm","size":2},{"name":"aaaacxi7gjs3iascvqjaabaaa4","size":5},{"name":"aaaacxi7gjs3iascvqjaabaaau","size":7},{"name":"aaaacxi7gjs3gascvqjaabaabi","size":5},{"name":"aaaacxi7gjs3iascvqjaabaaba","size":12},{"name":"aaaacxi7gjs3iascvqjaabaaay","size":4},{"name":"aaaacxi7gjs3gascvqjaabaaca","size":6},{"name":"aaaacxi7gjs3gascvqjaabaacq","size":4},{"name":"aaaacxi7gjs3eascvqjaabaaa4","size":4},{"name":"aaaacxi7gjs3iascvqjaabaabi","size":5},{"name":"aaaacxi7gjs3iascvqjaabaabe","size":1},{"name":"aaaacxi7gjs3gascvqjaabaaci","size":4},{"name":"aaaacxi7gjs3gascvqjaabaaai","size":4},{"name":"aaaacxi7gjs3gascvqjaabaaba","size":2},{"name":"aaaacxi7gjs3gascvqjaabaada","size":1},{"name":"aaaacxi7gjs3eascvqjaabaabe","size":2},{"name":"aaaacxi7gjs3eascvqjaabaacm","size":5},{"name":"aaaacxi7gjs3gascvqjaabaaau","size":2},{"name":"aaaacxi7gjs3iascvqjaabaaam","size":1},{"name":"aaaacxi7gjs3eascvqjaabaaba","size":1},{"name":"aaaacxi7gjs3gascvqjaabaab4","size":3}]}]}]},{"name":"3","children":[{"name":"p21.31","children":[{"name":"CACNA2D2","children":[{"name":"aaaacxi7gjufoascvqjaabaafi","size":2},{"name":"undefined","size":1},{"name":"aaaacxi7gjufqascvqjaabaaae","size":2},{"name":"aaaacxi7gjufqascvqjaabaadu","size":1},{"name":"aaaacxi7gjufqascvqjaabaaee","size":2},{"name":"aaaacxi7gjufqascvqjaabaace","size":1},{"name":"aaaacxi7gjufqascvqjaabaaca","size":2},{"name":"aaaacxi7gjufqascvqjaabaad4","size":2}]},{"name":"LARS2","children":[{"name":"aaaacxi7gj6pmascvqjaabaag4","size":30},{"name":"aaaacxi7gj6pmascvqjaabaagy","size":9},{"name":"aaaacxi7gj6pmascvqjaabaaga","size":16},{"name":"aaaacxi7gj6pmascvqjaabaagq","size":8},{"name":"aaaacxi7gj6pmascvqjaabaahi","size":11},{"name":"aaaacxi7gj6pmascvqjaabaagm","size":14},{"name":"aaaacxi7gj6pmascvqjaabaage","size":1},{"name":"undefined","size":10},{"name":"aaaacxi7gj6pmascvqjaabaaia","size":12},{"name":"aaaacxi7gj6pmascvqjaabaahy","size":15},{"name":"aaaacxi7gj6pmascvqjaabaafq","size":8},{"name":"aaaacxi7gj6pmascvqjaabaaha","size":10},{"name":"aaaacxi7gj6pmascvqjaabaagu","size":12},{"name":"aaaacxi7gj6pmascvqjaabaaf4","size":10},{"name":"aaaacxi7gj6pmascvqjaabaahq","size":10}]}]}]},{"name":"7","children":[{"name":"q22.1","children":[{"name":"TFR2","children":[{"name":"aaaacxi7gjz4wascvqjaabaadq","size":2},{"name":"aaaacxi7gjz4yascvqjaabaaaq","size":1},{"name":"undefined","size":24},{"name":"aaaacxi7gjz4wascvqjaabaacq","size":4},{"name":"aaaacxi7gjz4yascvqjaabaaay","size":1},{"name":"aaaacxi7gjz4wascvqjaabaadm","size":1},{"name":"aaaacxi7gjz4wascvqjaabaacy","size":2},{"name":"aaaacxi7gjz4yascvqjaabaabi","size":2},{"name":"aaaacxi7gjz4yascvqjaabaaau","size":6}]}]}]},{"name":"9","children":[{"name":"q22.2","children":[{"name":"SECISBP2","children":[{"name":"undefined","size":30},{"name":"aaaacxi7gkabkascvqjaabaaea","size":9},{"name":"aaaacxi7gkabkascvqjaabaae4","size":6},{"name":"aaaacxi7gkabkascvqjaabaaf4","size":6},{"name":"aaaacxi7gkabkascvqjaabaaey","size":7},{"name":"aaaacxi7gkabkascvqjaabaafq","size":10}]}]}]},{"name":"12","children":[{"name":"q15","children":[{"name":"NUP107","children":[{"name":"undefined","size":16},{"name":"aaaacxi7gj74sascvqjaabaaby","size":10},{"name":"aaaacxi7gj74sascvqjaabaade","size":7},{"name":"aaaacxi7gj74sascvqjaabaadq","size":9},{"name":"aaaacxi7gj74sascvqjaabaab4","size":7},{"name":"aaaacxi7gj74sascvqjaabaacu","size":9},{"name":"aaaacxi7gj74sascvqjaabaaem","size":7},{"name":"aaaacxi7gj74sascvqjaabaaa4","size":3},{"name":"aaaacxi7gj74sascvqjaabaace","size":8},{"name":"aaaacxi7gj74sascvqjaabaaee","size":7},{"name":"aaaacxi7gj74sascvqjaabaady","size":3},{"name":"aaaacxi7gj74sascvqjaabaabq","size":6},{"name":"aaaacxi7gj74sascvqjaabaaca","size":8},{"name":"aaaacxi7gj74sascvqjaabaabi","size":10},{"name":"aaaacxi7gj74sascvqjaabaabm","size":5},{"name":"aaaacxi7gj74sascvqjaabaacy","size":4},{"name":"aaaacxi7gj74sascvqjaabaada","size":10},{"name":"aaaacxi7gj74sascvqjaabaac4","size":6},{"name":"aaaacxi7gj74sascvqjaabaabu","size":9},{"name":"aaaacxi7gj74sascvqjaabaacm","size":7},{"name":"aaaacxi7gj74sascvqjaabaacq","size":3},{"name":"aaaacxi7gj74sascvqjaabaadu","size":3},{"name":"aaaacxi7gj74sascvqjaabaad4","size":1},{"name":"aaaacxi7gj74sascvqjaabaadi","size":4}]}]}]},{"name":"17","children":[{"name":"p13.1","children":[{"name":"SCO1","children":[{"name":"aaaacxi7gjwlyascvqjaabaacu","size":25},{"name":"aaaacxi7gjwlyascvqjaabaace","size":39}]}]}]},{"name":"19","children":[{"name":"q13.2","children":[{"name":"SPINT2","children":[{"name":"aaaacxi7gjuvmascvqjaabaadm","size":1},{"name":"undefined","size":5},{"name":"aaaacxi7gjuvmascvqjaabaadi","size":3},{"name":"aaaacxi7gjuvmascvqjaabaade","size":6},{"name":"aaaacxi7gjuvmascvqjaabaac4","size":6},{"name":"aaaacxi7gjuvmascvqjaabaada","size":4},{"name":"aaaacxi7gjuvmascvqjaabaadu","size":2},{"name":"aaaacxi7gjuvmascvqjaabaadq","size":1}]}]},{"name":"p13.12","children":[{"name":"PRKACA","children":[{"name":"aaaacxi7gkb32ascvqjaabaagi","size":9}]}]}]},{"name":"22","children":[{"name":"q13.33","children":[{"name":"SCO2","children":[{"name":"undefined","size":32},{"name":"aaaacxi7gka5qascvqjaabaaai","size":35},{"name":"aaaacxi7gka5oascvqjaabaacq","size":7},{"name":"aaaacxi7gka5qascvqjaabaaay","size":21},{"name":"aaaacxi7gj4eqascvqjaabaab4"},{"name":"aaaacxi7gka5oascvqjaabaacm"}]}]}]}]}
    var root = attrs.data.root

    console.log(root)
  root = d3.hierarchy(root)
      .sum(function(d) { return 1; })
      .sort(function(a, b) { return b.value - a.value; });

  var focus = root,
      nodes = pack(root).descendants(),
      view;

  var circle = g.selectAll("circle")
    .data(nodes)
    .enter().append("circle")
      .attr("class", function(d) { return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root"; })
      .style("fill", function(d) { return d.children ? color(d.depth) : null; })
      .on("click", function(d) { if (focus !== d) zoom(d), d3.event.stopPropagation(); });

  var text = g.selectAll("text")
    .data(nodes)
    .enter().append("text")
      .attr("class", "label")
      .style("text-anchor", "middle")
      .style("fill-opacity", function(d) { return d.parent === root ? 1 : 0; })
      .style("display", function(d) { return d.parent === root ? "inline" : "none"; })
      .text(function(d) { return truncate(d.data.name); });

  var node = g.selectAll("circle,text");

  svg
      .on("click", function() { zoom(root); });

  zoomTo([root.x, root.y, root.r * 2 + margin]);

  function zoom(d) {
    var focus0 = focus; focus = d;

    var transition = d3.transition()
        .duration(d3.event.altKey ? 7500 : 750)
        .tween("zoom", function(d) {
          var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin]);
          return function(t) { zoomTo(i(t)); };
        });

    transition.selectAll("text")
      .filter(function(d) { return d.parent === focus || this.style.display === "inline"; })
        .style("fill-opacity", function(d) { return d.parent === focus ? 1 : 0; })
        .style("background-color", "blue")
        .on("start", function(d) { if (d.parent === focus) this.style.display = "inline"; })
        .on("end", function(d) { if (d.parent !== focus) this.style.display = "none"; });
  }

    function truncate(str) {
        let length = 20;
        let ending = '...';

        if (str && str.length > length) {
            return str.substring(0, length - ending.length) + ending;
        } else {
            return str;
        }
    }

  function zoomTo(v) {
    var k = diameter / v[2]; view = v;

    node.attr("transform", function(d) { return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")"; });
    circle.attr("r", function(d) { return d.r * k; });
  }
});









      updateData = function () {
        main.run();
      }


     


  };

  d3.selection.prototype.patternify = function (params) {
    var container = this;
    var selector = params.selector;
    var elementTag = params.tag;
    var data = params.data || [selector];
    var selection = container.selectAll('.' + selector).data(data)
    selection.exit().remove();
    selection = selection.enter().append(elementTag).merge(selection)
    selection.attr('class', selector);
    return selection;
  }

  d3.radial = function () {
    return function setProportions(root) {
      recurse(root, 0, 1);
      function recurse(node, min, max) {
        node.proportion = (max + min) / 2;
        if (!node.x) {
          if (node.parent) {
            node.x = node.parent.x;
          } else {
            node.x = 0;
          }
        }
        if (!node.y) {
          if (node.parent) {
            node.y = node.parent.y;
          } else {
            node.y = 0;
          }
        }
        if (node.children) {
          var offset = (max - min) / node.children.length;
          node.children.forEach(function (child, i, arr) {
            var newMin = min + offset * i;
            var newMax = newMin + offset;
            recurse(child, newMin, newMax);
          });
        }
      }
    }
  }
  Array.prototype.orderBy = function (func) {
    this.sort((a, b) => {
      var a = func(a);
      var b = func(b);
      if (typeof a === 'string' || a instanceof String) {
        return a.localeCompare(b);
      }
      return a - b;
    });
    return this;
  }

  Object.keys(attrs).forEach(key => {
    return main[key] = function (_) {
      var string = `attrs['${key}'] = _`;
      if (!arguments.length) { return eval(` attrs['${key}'];`); }
      eval(string);
      return main;
    };
  });

  main.attrs = attrs;

  main.hoverCallback = function(value) {
    attrs.hoverCallback = value
    return main
  }

  main.data = function (value) {
    if (!arguments.length) return attrs.data;
    attrs.data = value;
    if (typeof updateData === 'function') {
      updateData();
    }
    return main;
  }

  main.run = function () {
    d3.selectAll(attrs.container).call(main);
    return main;
  }

  main.filter = function (filterParams) {
    if (!arguments.length) return attrs.filterParams;
    attrs.filterParams = filterParams;
    if (typeof filter === 'function') {
      filter();
    }
    return main;
  }

  return main;
}
