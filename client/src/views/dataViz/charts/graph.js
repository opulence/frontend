export function renderChartCollapsibleNetwork(params) {
  var attrs = {
    id: 'id' + Math.floor(Math.random() * 1000000),
    svgWidth: 960,
    svgHeight: 600,
    marginTop: 0,
    marginBottom: 5,
    marginRight: 0,
    marginLeft: 30,
    nodeRadius: 18,
    container: 'body',
    distance: 100,
    hiddenChildLevel: 1,
    linkColor: '#0F4C81',
    hoverOpacity: 0.2,
    maxTextDisplayZoomLevel: 1,
    lineStrokeWidth: 1.5,
    data: null,
    hoverCallback: null,
  };


  var updateData;
  var filter;

  var main = function (selection) {
    selection.each(function scope() {
      //calculated properties
      var calc = {}
      calc.chartLeftMargin = attrs.marginLeft;
      calc.chartTopMargin = attrs.marginTop;
      calc.chartWidth = attrs.svgWidth - attrs.marginRight - calc.chartLeftMargin;
      calc.chartHeight = attrs.svgHeight - attrs.marginBottom - calc.chartTopMargin;

      //########################## HIERARCHY STUFF  #########################
      var hierarchy = {};
      hierarchy.root = d3.hierarchy(attrs.data.root);


      //###########################   BEHAVIORS #########################
      var behaviors = {};
      behaviors.zoom = d3.zoom().scaleExtent([0.75, 100, 8]).on('zoom', zoomed);
      behaviors.drag = d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended);

      //###########################   LAYOUTS #########################
      var layouts = {};

      // custom radial kayout
      layouts.radial = d3.radial();




      //###########################   FORCE STUFF #########################
      var force = {};
      force.link = d3.forceLink().id(d => d.data.name);
      force.charge = d3.forceManyBody()
      force.center = d3.forceCenter(calc.chartWidth / 2, calc.chartHeight / 2)

      // prevent collide
      force.collide = d3.forceCollide().radius(d => {

        // if parent has many children, reduce collide strength
        if (d.parent) {
          if (d.parent.children.length > 10) {

            // also slow down node movement
            slowDownNodes();
            return 7;
          }
        }

        // increase collide strength 
        if (d.children && d.depth > 2) {
          return attrs.nodeRadius;
        }
        return attrs.nodeRadius * 2;
      });

      //manually set x positions (which is calculated using custom radial layout)
      force.x = d3.forceX()
        .strength(0.5)
        .x(function (d, i) {

          // if node does not have children and is channel (depth=2) , then position it on parent's coordinate
          if (!d.children && d.depth > 2) {
            if (d.parent) {
              d = d.parent
            }
          }

          // custom circle projection -  radius will be -  (d.depth - 1) * 150
          return projectCircle(d.proportion, (d.depth - 1) * 150)[0];
        });


      //manually set y positions (which is calculated using d3.cluster)
      force.y = d3.forceY()
        .strength(0.5)
        .y(function (d, i) {

          // if node does not have children and is channel (depth=2) , then position it on parent's coordinate
          if (!d.children && d.depth > 2) {
            if (d.parent) {
              d = d.parent
            }
          }

          // custom circle projection -  radius will be -  (d.depth - 1) * 150
          return projectCircle(d.proportion, (d.depth - 1) * 150)[1];
        })


      //---------------------------------  INITIALISE FORCE SIMULATION ----------------------------

      // get based on top parameter simulation
      force.simulation = d3.forceSimulation()
        .force('link', force.link)
        .force('charge', force.charge)
        .force('center', force.center)
        .force("collide", force.collide)
        .force('x', force.x)
        .force('y', force.y)


      //###########################   HIERARCHY STUFF #########################

      // flatten root 
      var arr = flatten(hierarchy.root);

      // hide members based on their depth
      arr.forEach(d => {
        if (d.depth > attrs.hiddenChildLevel) {
          d._children = d.children;
          d.children = null;
        }
      })

      //####################################  DRAWINGS #######################


      //drawing containers
      var container = d3.select(this);

      //add svg
      var svg = container.patternify({ tag: 'svg', selector: 'svg-chart-container' })
        .attr('width', attrs.svgWidth)
        .attr('height', attrs.svgHeight)
        .call(behaviors.zoom)

      //add container g element
      var chart = svg.patternify({ tag: 'g', selector: 'chart' })
        .attr('transform', 'translate(' + (calc.chartLeftMargin) + ',' + calc.chartTopMargin + ')');



      //################################   Chart Content Drawing ##################################




      var linksWrapper = chart.patternify({ tag: 'g', selector: 'links-wrapper' })
      var nodesWrapper = chart.patternify({ tag: 'g', selector: 'nodes-wrapper' })
      var nodes, links, enteredNodes;
      // reusable function which updates visual based on data change

      update();

      function update(clickedNode) {
        layouts.radial(hierarchy.root);

        //nodes and links array
        var nodesArr = flatten(hierarchy.root, true)
          .orderBy(d => d.depth)
          .filter(d => !d.hidden);

        nodesArr.forEach(function (d, i) {
          if (clickedNode && clickedNode.id == (d.parent && d.parent.id)) {
            d.x = d.parent.x;
            d.y = d.parent.y;
          }
        });


        linksArr = attrs.data.links


        function rm_hidden(d) {
            let desc = 0
            function contains_id(v, i, a) {
                if (d.source.data && d.target.data && d.source.data.name && d.target.data.name) {
                    if  ((v.data.name === d.target.data.name) || (v.data.name === d.source.data.name)) {
                        desc = desc + 1
                    }
                } else if ((v.data.name === d.target) || (v.data.name === d.source)) {
                    desc = desc + 1
                }
            }
            nodesArr.forEach(contains_id)
            return desc >= 2
        }
        let linksArr = linksArr.filter(rm_hidden)



        links = linksWrapper.selectAll('.link').data(linksArr);
        links.exit().remove();
        links = links.enter()
          .append('line')
          .attr('class', 'link')
          .merge(links)


        links.attr('stroke', attrs.linkColor).attr('stroke-width', attrs.lineStrokeWidth)
        nodes = nodesWrapper.selectAll('.node').data(nodesArr, d => d.id);
        var exited = nodes.exit().remove();
        var enteredNodes = nodes.enter()
          .append('g')
          .attr('class', 'node')
        enteredNodes.on('click', nodeClick)
          .on('mouseenter', nodeMouseEnter)
          .on('mouseleave', nodeMouseLeave)
          .call(behaviors.drag)



        //channels grandchildren
        var channelsGrandchildren = enteredNodes
          .append("circle")
          .attr('r', 7)
          .attr('stroke-width', 5)
          .attr('stroke', attrs.nodeStroke)



        nodes = enteredNodes.merge(nodes);

          enteredNodes.append("text")
        .attr("class","fa")
        .attr("text-anchor", "middle")
        .attr("y", 6)
        .attr("fill","#0F4C81")
        .attr('transform','scale(1.6)')
        .text(d => d.data.family)


          enteredNodes.append('text')
          .text(d => d.data.name)
          .attr("x", 0)
          .attr("y", 42)
          .style("font-size", "19px")
          .attr('class', 'text')
          .attr("text-anchor", "middle")



        force.simulation.nodes(nodesArr)
          .on('tick', ticked);

        force.simulation.force("link").links(linksArr).distance(100).strength(d => 1);
      }


      function zoomed() {

        var transform = d3.event.transform;
        attrs.lastTransform = transform;

        chart.attr('transform', transform)
       
        svg.selectAll('.node').attr("transform", function (d) { return `translate(${d.x},${d.y}) scale(${1 / (attrs.lastTransform ? attrs.lastTransform.k : 1)})`; });
        svg.selectAll('.link').attr("stroke-width", attrs.lineStrokeWidth / (attrs.lastTransform ? attrs.lastTransform.k : 1));

        if (transform.k < attrs.maxTextDisplayZoomLevel) {
          svg.selectAll('.node-texts');
        } else {
          svg.selectAll('.node-texts').style('display', 'initial');
        }
      }


      function ticked() {
        links.attr("x1", function (d) { return d.source.x; })
          .attr("y1", function (d) { return d.source.y; })
          .attr("x2", function (d) { return d.target.x; })
          .attr("y2", function (d) { return d.target.y; });


        svg.selectAll('.node').attr("transform", function (d) { return `translate(${d.x},${d.y}) scale(${1 / (attrs.lastTransform ? attrs.lastTransform.k : 1)})`; });
      }
      function dragstarted(d) {
   // d.fx = d.x;
   // d.fy = d.y;
        nodes.each(d => { d.fx = null; d.fy = null })


      }

      function dragged(d) {
  // d.fx = d3.event.x;
  // d.fy = d3.event.y;
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      }

     
      function dragended(d) {
  // d.fx = d.x;
  // d.fy = d.y;
      }

      function nodeMouseEnter(d) {
        return
        attrs.hoverCallback(d)
        var node = d3.select(this);
        var links = hierarchy.root.links();
        var connectedLinks = links.filter(l => l.source.id == d.id || l.target.id == d.id);
        var linkedNodes = connectedLinks.map(s => s.source.id).concat(connectedLinks.map(d => d.target.id))
        nodesWrapper.selectAll('.node')
          .filter(n => linkedNodes.indexOf(n.id) == -1)
          .attr('opacity', attrs.hoverOpacity);
        linksWrapper.selectAll('.link').attr('opacity', attrs.hoverOpacity);


        linksWrapper.selectAll('.link')
          .filter(l => l.source.id == d.id || l.target.id == d.id)
          .attr('opacity', 1)
          .attr('stroke', "#B37514")

        

      }

      function nodeMouseLeave(d) {
        nodesWrapper.selectAll('.node').attr('opacity', 1);
        linksWrapper.selectAll('.link').attr('opacity', 1).attr('stroke', attrs.linkColor)
      }

      function nodeClick(d) {


        d3.select(this).classed("fixed", d.fixed = false);
        if (d.children) {
          d._children = d.children;
          d.children = null;
          update();
          force.simulation.restart();
          force.simulation.alphaTarget(0.95);
        } else if (d._children) {
          d.children = d._children;
          d._children = null;
          update(d);
          force.simulation.restart();
          force.simulation.alphaTarget(0.95);
        }
        freeNodes();


      }

      updateData = function () {
        main.run();
      }

      function slowDownNodes() {
        force.simulation.alphaTarget(0.05);
      };

      function speedUpNodes() {
        force.simulation.alphaTarget(0.45);
      }

      function freeNodes() {
        d3.selectAll('.node').each(n => { n.fx = null; n.fy = null; })
      }

      function projectCircle(value, radius) {
        var r = radius || 0;
        var corner = value * 2 * Math.PI;
        return [Math.sin(corner) * r, -Math.cos(corner) * r]

      }

     
      function flatten(root, clustered) {
        var nodesArray = [];
        var i = 0;
        function recurse(node, depth) {
          if (node.children)
            node.children.forEach(function (child) {
              recurse(child, depth + 1);
            });
          if (!node.id) node.id = ++i;
          else ++i;
          node.depth = depth;
          if (clustered) {
            if (!node.cluster) {
              node.cluster = { x: node.x, y: node.y }
            }
          }
          nodesArray.push(node);
        }
        recurse(root, 1);
        return nodesArray;
      }


    });
  };

  d3.selection.prototype.patternify = function (params) {
    var container = this;
    var selector = params.selector;
    var elementTag = params.tag;
    var data = params.data || [selector];
    var selection = container.selectAll('.' + selector).data(data)
    selection.exit().remove();
    selection = selection.enter().append(elementTag).merge(selection)
    selection.attr('class', selector);
    return selection;
  }

  d3.radial = function () {
    return function setProportions(root) {
      recurse(root, 0, 1);
      function recurse(node, min, max) {
        node.proportion = (max + min) / 2;
        if (!node.x) {

          // if node has parent, match entered node positions to it's parent
          if (node.parent) {
            node.x = node.parent.x;
          } else {
            node.x = 0;
          }
        }

        // if node had parent, match entered node positions to it's parent
        if (!node.y) {
          if (node.parent) {
            node.y = node.parent.y;
          } else {
            node.y = 0;
          }
        }

        //recursively do the same for children
        if (node.children) {
          var offset = (max - min) / node.children.length;
          node.children.forEach(function (child, i, arr) {
            var newMin = min + offset * i;
            var newMax = newMin + offset;
            recurse(child, newMin, newMax);
          });
        }
      }
    }
  }



  Array.prototype.orderBy = function (func) {
    this.sort((a, b) => {
      var a = func(a);
      var b = func(b);
      if (typeof a === 'string' || a instanceof String) {
        return a.localeCompare(b);
      }
      return a - b;
    });
    return this;
  }

  Object.keys(attrs).forEach(key => {
    return main[key] = function (_) {
      var string = `attrs['${key}'] = _`;
      if (!arguments.length) { return eval(` attrs['${key}'];`); }
      eval(string);
      return main;
    };
  });

  main.attrs = attrs;

  main.hoverCallback = function(value) {
    attrs.hoverCallback = value
    return main
  }

  main.data = function (value) {
    if (!arguments.length) return attrs.data;
    attrs.data = value;
    if (typeof updateData === 'function') {
      updateData();
    }
    return main;
  }

  main.run = function () {
    d3.selectAll(attrs.container).call(main);
    return main;
  }

  main.filter = function (filterParams) {
    if (!arguments.length) return attrs.filterParams;
    attrs.filterParams = filterParams;
    if (typeof filter === 'function') {
      filter();
    }
    return main;
  }

  return main;
}
