export function renderTreeCollapsible(params) {
  var attrs = {
    id: 'id' + Math.floor(Math.random() * 1000000),
    svgWidth: 960,
    svgHeight: 600,
    marginTop: 0,
    marginBottom: 5,
    marginRight: 0,
    marginLeft: 30,
    nodeRadius: 18,
    container: 'body',
    distance: 50,
    hiddenChildLevel: 1,
    linkColor: '#0F4C81',
    hoverOpacity: 0.2,
    maxTextDisplayZoomLevel: 1,
    lineStrokeWidth: 1.5,
    data: null,
    hoverCallback: null,
  };


  var updateData;
  var filter;

  var main = function (selection) {
    selection.each(function scope() {






        console.log("!!!", attrs.data)
    
    var totalNodes = 0;
    var maxLabelLength = 0;
    var selectedNode = null;
    var draggingNode = null;
    var panSpeed = 200;
    var panBoundary = 20; // Within 20px from edges will pan when dragging.
    var i = 0;
    var duration = 750;
    var treeData = attrs.data.root
    var root = d3.hierarchy(treeData, function(d) { return d.children; });


      var calc = {}
      calc.chartLeftMargin = attrs.marginLeft;
      calc.chartTopMargin = attrs.marginTop;
      calc.chartWidth = attrs.svgWidth - attrs.marginRight - calc.chartLeftMargin;
      calc.chartHeight = attrs.svgHeight - attrs.marginBottom - calc.chartTopMargin;
    var viewerWidth = $(document).width(); 
    var viewerHeight = $(document).height();

    var treemap;

    function visit(parent, visitFn, childrenFn) {
        if (!parent) return;
        visitFn(parent);
        var children = childrenFn(parent);
        if (children) {
            var count = children.length;
            for (var i = 0; i < count; i++) {
                visit(children[i], visitFn, childrenFn);
            }
        }
    }

    visit(treeData, function(d) { totalNodes++;
                                  maxLabelLength = Math.max(d.name.length, maxLabelLength); }, 
                                  function(d) { return d.children && d.children.length > 0 ? d.children : null; });
    function sortTree() {

    }
    sortTree();
    function pan(domNode, direction) {
        var speed = panSpeed;
        if (panTimer) {
            clearTimeout(panTimer);
            translateCoords = d3.transform(svgGroup.attr("transform"));
            if (direction == 'left' || direction == 'right') {
                translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                translateY = translateCoords.translate[1];
            } else if (direction == 'up' || direction == 'down') {
                translateX = translateCoords.translate[0];
                translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
            }
            scaleX = translateCoords.scale[0];
            scaleY = translateCoords.scale[1];
            scale = zoomListener.scale();
            svgGroup.transition().attr("transform", "translate(" + translateX+ "," + translateY + ")scale(" + scale + ")");
            d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX / 3+ "," + translateY / 3 + ")");
            zoomListener.scale(zoomListener.scale());
            zoomListener.translate([translateX, translateY]);
            panTimer = setTimeout(function() {
                pan(domNode, speed, direction);
            }, 50);
        }
    }
    function zoom() {
            if(d3.event.transform != null) {
        svgGroup.attr("transform", d3.event.transform );
            }
    }

    function centerNode(source) {
      var t = d3.zoomTransform(baseSvg.node());
      var x = -source.y0;
      var y = -source.x0;
      var x = x * t.k + viewerWidth / 2;
      var y = y * t.k + viewerHeight / 2;
      d3.select('svg').transition().duration(duration)
                                   .call( zoomListener.transform, d3.zoomIdentity.translate(x,y).scale(t.k) );
}
    var zoomListener = d3.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);
    function initiateDrag(d, domNode) {
        draggingNode = d;
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
        d3.select(domNode).attr('class', 'node activeDrag');

        svgGroup.selectAll("g.node").sort(function(a, b) { 
            if (a.id != draggingNode.id) return 1; 
            else return -1; 
        });
        if (nodes.length > 1) {
            links = tree.links(nodes);
            nodePaths = svgGroup.selectAll("path.link").data(links, function(d) { return d.id; }).remove();
            nodesExit = svgGroup.selectAll("g.node").data(nodes, function(d) { return d.id; })
                                                                .filter(function(d, i) { if (d.id == draggingNode.id) {
                                                                               return false;
                                                                             }
                                                                               return true;
                                                                             }).remove();
        }
        parentLink = tree.links(tree.nodes(draggingNode.parent));
        svgGroup.selectAll('path.link').filter(function(d, i) { if (d.id == draggingNode.id) {
                                                                  return true;
                                                                }
                                                                  return false;
                                                              }).remove();

        dragStarted = null;
    }

    var baseSvg = d3.select(attrs.container).append("svg").attr("width", viewerWidth)
                                                            .attr("height", viewerHeight)
                                                            .attr("class", "overlay")
                                                            .call(zoomListener);

    function collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }

    function expand(d) {
        if (d._children) {
            d.children = d._children;
            d.children.forEach(expand);
            d._children = null;
        }
    }

    var overCircle = function(d) {
        selectedNode = d;
        updateTempConnector();
    };
    var outCircle = function(d) {
        selectedNode = null;
        updateTempConnector();
    };

    var updateTempConnector = function() {
        var data = [];
        if (draggingNode !== null && selectedNode !== null) {
            data = [{
                source: {
                    x: selectedNode.y0,
                    y: selectedNode.x0
                },
                target: {
                    x: draggingNode.y0,
                    y: draggingNode.x0
                }
            }];
        }
        var link = svgGroup.selectAll(".templink").data(data);

        link.enter().append("path")
            .attr("class", "templink")
            .attr("d", function(d){ var o = {x: source.x0, y: source.y0 };
                                            return diagonal(o, o);  })
            .attr('pointer-events', 'none');

        link.attr("d", function(d){ var o = {x: source.x0, y: source.y0 };
                                            return diagonal(o, o);  });

        link.exit().remove();
    };

    function toggleChildren(d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else if (d._children) {
            d.children = d._children;
            d._children = null;
        }
        return d;
    }


    function click(d) {
        if (d3.event.defaultPrevented) return;
        d = toggleChildren(d);
        update(d);
        centerNode(d);
    }
        
        function diagonal(s, d) {

            if(s != null &&
               d != null) {
              var path = "M " + s.y + " " + s.x
                            + " C " + (( s.y + d.y ) / 2) + " " + s.x + ","
                            + (( s.y + d.y ) / 2) + " " + d.x + ","
                            + " " + d.y + " " + d.x;

              return path;
            }
        }

    function update(source) {
        var levelWidth = [1];
        var childCount = function(level, n) {

            if (n.children && n.children.length > 0) {
                if (levelWidth.length <= level + 1) levelWidth.push(0);

                levelWidth[level + 1] += n.children.length;
                n.children.forEach(function(d) {
                    childCount(level + 1, d);
                });
            }
        };
        childCount(0, root);
        var newHeight = d3.max(levelWidth) * 25;
            treemap = d3.tree().size([newHeight, viewerWidth]);
            var treeData = treemap(root);
        var nodes = treeData.descendants(),
                links = treeData.descendants().slice(1);

        nodes.forEach(function(d) {
            d.y = (d.depth * (maxLabelLength * 10));

        });
        var node = svgGroup.selectAll("g.node").data(nodes, function(d) { return d.id || (d.id = ++i); });
        var nodeEnter = node.enter().append("g")//.call(dragListener)
                                    .attr("class", "node")
                                    .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
                                    .on('click', click);
        nodeEnter.append("circle").attr('class', 'nodeCircle')
                                  .attr("r", 8.5)
                                  .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
        nodeEnter.append("text").attr("x", function(d) { return d.children || d._children ? -10 : 10; })
                                .attr("dy", ".35em")
                                .attr('class', 'nodeText')
                                .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
                                .text(function(d) { return d.data.name; })
                                .style("fill-opacity", 0);

        node.select('text').attr("x", function(d) { return d.children || d._children ? -10 : 10; })
                           .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
                           .text(function(d) { return d.data.name; });
        node.select("circle.nodeCircle").attr("r", 8.5).style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
                var nodeUpdate = nodeEnter.merge(node);
                nodeUpdate.transition().duration(duration).attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });
        nodeUpdate.select("text").style("fill-opacity", 1);
        var nodeExit = node.exit().transition().duration(duration).attr("transform", function(d) { return "translate(" + source.y + "," + source.x +")";}).remove();
        nodeExit.select("circle").attr("r", 0);
        nodeExit.select("text").style("fill-opacity", 0);
        var link = svgGroup.selectAll("path.link").data(links, function(d) { return d.id; });
        var linkEnter = link.enter().insert("path", "g").attr("class", "link")
                                                        .attr("d", function(d) { var o = { x: source.x0, y: source.y0 };
                                                                                                                                                                 return diagonal(o, o); })
                var linkUpdate = linkEnter.merge(link);
        linkUpdate.transition().duration(duration).attr('d', function(d){ return diagonal(d, d.parent) });
        var linkExit = link.exit().transition().duration(duration).attr("d", function(d) { var o = { x: source.x, y: source.y };
                                                                                           return diagonal(o, o); })
                                                   .remove();
        nodes.forEach(function(d) { d.x0 = d.x;
                                    d.y0 = d.y; });
    }

    var svgGroup = baseSvg.append("g");

    root.x0 = viewerHeight / 2;
    root.x0 = 200;
    root.y0 = 50;

    update(root);
    centerNode(root);
});







      updateData = function () {
        main.run();
      }


     


    
  };

  d3.selection.prototype.patternify = function (params) {
    var container = this;
    var selector = params.selector;
    var elementTag = params.tag;
    var data = params.data || [selector];
    var selection = container.selectAll('.' + selector).data(data)
    selection.exit().remove();
    selection = selection.enter().append(elementTag).merge(selection)
    selection.attr('class', selector);
    return selection;
  }

  d3.radial = function () {
    return function setProportions(root) {
      recurse(root, 0, 1);
      function recurse(node, min, max) {
        node.proportion = (max + min) / 2;
        if (!node.x) {
          if (node.parent) {
            node.x = node.parent.x;
          } else {
            node.x = 0;
          }
        }
        if (!node.y) {
          if (node.parent) {
            node.y = node.parent.y;
          } else {
            node.y = 0;
          }
        }
        if (node.children) {
          var offset = (max - min) / node.children.length;
          node.children.forEach(function (child, i, arr) {
            var newMin = min + offset * i;
            var newMax = newMin + offset;
            recurse(child, newMin, newMax);
          });
        }
      }
    }
  }
  Array.prototype.orderBy = function (func) {
    this.sort((a, b) => {
      var a = func(a);
      var b = func(b);
      if (typeof a === 'string' || a instanceof String) {
        return a.localeCompare(b);
      }
      return a - b;
    });
    return this;
  }

  Object.keys(attrs).forEach(key => {
    return main[key] = function (_) {
      var string = `attrs['${key}'] = _`;
      if (!arguments.length) { return eval(` attrs['${key}'];`); }
      eval(string);
      return main;
    };
  });

  main.attrs = attrs;

  main.hoverCallback = function(value) {
    attrs.hoverCallback = value
    return main
  }

  main.data = function (value) {
    if (!arguments.length) return attrs.data;
    attrs.data = value;
    if (typeof updateData === 'function') {
      updateData();
    }
    return main;
  }

  main.run = function () {
    d3.selectAll(attrs.container).call(main);
    return main;
  }

  main.filter = function (filterParams) {
    if (!arguments.length) return attrs.filterParams;
    attrs.filterParams = filterParams;
    if (typeof filter === 'function') {
      filter();
    }
    return main;
  }

  return main;
}
