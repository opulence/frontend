import os
from flask import Flask, current_app, send_file

from .factory import factory

app = factory.set_flask()
factory.set_celery()

from .api import api_bp

factory.register(api_bp)


@app.route('/')
def index_client():
    dist_dir = current_app.config['DIST_DIR']
    entry = os.path.join(dist_dir, 'index.html')
    print(entry)
    return send_file(entry)