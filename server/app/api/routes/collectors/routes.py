import json
from flask_restplus import Resource
from flask import request, jsonify, abort
from app.api import api_rest

from . import tasks
from app.api.common.exceptions import TaskTimeoutError



@api_rest.route('/collectors')
class Collectors(Resource):

    def get(self):
        try:
            res = tasks.get()
        except TaskTimeoutError:
            abort(408)
        else:
            return jsonify(json.loads(res))
