from app.factory import factory
from app.api.common.utils import sync_call
from dynaconf import settings

engine = factory.engine
timeout = settings.CELERY_TASKS_TIMEOUT

def get():
    return sync_call(
        engine,
        "engine:collectors.get",
        timeout=timeout
    )
# @api_rest.route('/collectors')
# class Collectors(Resource):

#     def get(self):
#         res = engine.send_task("engine:get_collectors").get()
#         return jsonify(json.loads(res))

# @api_rest.route('/collectors/reload')
# class CollectorsReload(Resource):

#     def get(self):
#         print("!!!!!!!!!!!!!!")
#         res = engine.send_task("engine:load_collectors", [True]).get()
#         return 200

# @api_rest.route('/collectors/execute')
# class CollectorsExecute(Resource):

#     def post(self):
#         data = request.get_json()
#         collector_name = data.get('collector_name')
#         facts = data.get('facts')
#         return engine.send_task("engine:execute_collector", [collector_name, facts]).get()
