import json
from flask_restplus import Resource
from flask import request, jsonify, abort
from app.api import api_rest

from . import tasks
from app.api.common.exceptions import TaskTimeoutError



@api_rest.route('/facts')
class Facts(Resource):

    def get(self):
        try:
            facts = tasks.get()
        except TaskTimeoutError:
            abort(408)
        else:
            res = {}
            for fact in json.loads(facts):
                category = fact["plugin_data"]["category"]
                if not category in res:
                    res[category] = []
                res[category].append(fact)
            return jsonify(res)
