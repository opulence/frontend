from app.factory import factory
from app.api.common.utils import sync_call
from dynaconf import settings

engine = factory.engine
timeout = settings.CELERY_TASKS_TIMEOUT

def get():
    return sync_call(
        engine,
        "engine:facts.get",
        timeout=timeout
    )
