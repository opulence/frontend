from datetime import datetime
from flask import request
from flask_restplus import Resource

from .. import api_rest
from ..security import SecureResource


@api_rest.route('/resource/<string:resource_id>')
class ResourceOne(Resource):

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}

    def post(self, resource_id):
        json_payload = request.json
        return {'timestamp': json_payload}, 201

@api_rest.route('/secure-resource/<string:resource_id>')
class SecureResourceOne(SecureResource):

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}
