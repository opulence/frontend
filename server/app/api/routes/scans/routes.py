import json
from celery import group
from flask_restplus import Resource
from flask import request, jsonify, abort
from app.api import api_rest

from . import tasks
from app.api.common.exceptions import TaskTimeoutError


@api_rest.route('/scans/launch')
class QuickScan(Resource):

    def post(self):
        data = request.get_json()
        if "scan_type" not in data:
            return "No scan type provided", 400
        if data["scan_type"] == "Full Scan":
            try:
                res = tasks.full_scan(data["inputs"])
            except TaskTimeoutError:
                abort(408)
            else:
                return res
        else:
            return "Scan type not allowed", 403


@api_rest.route('/scans/quick_scan')
class QuickScan(Resource):

    def post(self):
        data = request.get_json()
        collector_name = data.get('collector_name')
        facts = data.get('facts')

        try:
            res = tasks.quick_scan(collector_name, facts)
        except TaskTimeoutError:
            abort(408)
        else:
            return res

@api_rest.route('/scans')
class Scans(Resource):

    def get(self):
        try:
            scans = json.loads(tasks.list())
            g = group(tasks.get_signature(scan["_id"]) for scan in scans)
            results = g().get()
        except TaskTimeoutError:
            abort(408)
        else:
            return jsonify(results)

@api_rest.route('/scans/remove')
class ScansRemove(Resource):

    def post(self):
        data = request.get_json()
        result_id = data.get("id")

        tasks.remove(result_id)
        return jsonify(success=True)


@api_rest.route('/scans/<string:scan_id>/tree')
class Tree(Resource):

    def get(self, scan_id):
        try:
            res = tasks.tree(scan_id)
        except TaskTimeoutError:
            abort(408)
        else:
            return res

@api_rest.route('/scans/<string:scan_id>/flat')
class FlatResult(Resource):

    def get(self, scan_id):
        try:
            res = tasks.flat(scan_id)
        except TaskTimeoutError:
            abort(408)
        else:
            return res