from app.factory import factory
from app.api.common.utils import sync_call, async_call
from dynaconf import settings

engine = factory.engine
timeout = settings.CELERY_TASKS_TIMEOUT

def quick_scan(collector_name, facts):
    return sync_call(
        engine,
        "engine:scan.quick",
        args=[collector_name, facts],
        timeout=timeout
    )

def full_scan(inputs):
    return sync_call(
        engine,
        "engine:scan.full",
        args=[inputs],
        timeout=timeout
    )

def get(scan_id):
    return sync_call(
        engine,
        "engine:scan.get",
        args=[scan_id],
        timeout=timeout
    )

def get_signature(scan_id):
    return engine.signature("engine:scan.get", args=[scan_id], immutable=True)

def list():
    return sync_call(
        engine,
        "engine:scan.list",
        timeout=timeout
    )

def remove(id):
    return async_call(
        engine,
        "engine:scan.remove",
        args=[id]
    )

def tree(scan_id):
    return sync_call(
        engine,
        "engine:scan.tree",
        args=[scan_id],
        timeout=timeout
    )

def flat(scan_id):
    return sync_call(
        engine,
        "engine:scan.flat_result",
        args=[scan_id],
        timeout=timeout
    )