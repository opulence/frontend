from functools import wraps
from flask import request
from flask_restplus import abort
from flask_restplus import Resource


def require_auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if request.headers.get('authorization'):
            return func(*args, **kwargs)
        else:
            return abort(401)
    return wrapper


class SecureResource(Resource):
    method_decorators = [require_auth]