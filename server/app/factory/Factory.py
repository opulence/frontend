import os
from flask import Flask
from kombu import Queue
from kombu import Exchange
from celery import Celery
from flask_cors import CORS

from dynaconf import settings

# TODO: singleton ?
class FactoryClass:

    def __init__(self):
        self.flask = None
        self.engine = None

    def set_flask(self, **kwargs):
        self.flask = Flask(__name__, static_folder='../../client/dist/static')
        CORS(self.flask, resources={r"/api/*": {"origins": "*"}})
        self.flask.config.from_object(settings.FLASK_CONFIG)
        return self.flask

    def _config_celery(self, config, **kwargs):
        task_queues = {"task_queues" : (
            Queue(
                config.task_default_queue,
                Exchange(config.task_default_queue),
                routing_key=config.task_default_queue
            ),
        )}
        app = Celery(config.task_default_queue, **kwargs)
        app.conf.update(**config)
        app.conf.update(task_queues)
        return app

    def set_celery(self, **kwargs):
        self.engine = self._config_celery(settings.ENGINE_SERVER, **kwargs)
        return self.engine

    def register(self, blueprint):
        self.flask.register_blueprint(blueprint)
