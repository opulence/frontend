from .Factory import FactoryClass

factory = FactoryClass()

__all__ = [factory]