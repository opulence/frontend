#!/usr/bin/env python3

from app import factory

application = factory.flask

#frontend_app = factory.frontend_app

if __name__ == '__main__':
    print("URLS", application.url_map)
    application.run(port=8000)
